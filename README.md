# ZWRecordTextTool

### 介绍
iOS开发中用来在App沙盒Library路径下记录文本数据（支持修改路径），会根据时间生成对应命名的文本文件（支持文件名按指定时间的样式命名）。


### 安装教程

cd到你的工程路径，在Podfile中添加以下命令
```
pod 'ZWRecordTextTool', :tag => '3.0.0', :git =>'https://gitee.com/MrZhang-iOS/ZWRecordTextTool.git', :branch => 'master'
```

最好先执行以下脚本查询最新的版本
```
pod search ZWRecordTextTool
```

然后执行
```
pod install
```


### 使用说明

在需要进行日志记录的类中引用库
```
#import <ZWRecordTextTool/ZWRecordTextTool.h>
```

方法调用示例：
```
[ZWRecordTextTool recordText:@"你的日志" completedHanle:nil]; 
```

如果需要存储在自定义的文件名中也可以调用：
```
[ZWRecordTextTool recordText:@"你的日志" fileName:@"YourFileName.text" completedHanle:nil];
```



*如果有其他问题可以留言，谢谢！*