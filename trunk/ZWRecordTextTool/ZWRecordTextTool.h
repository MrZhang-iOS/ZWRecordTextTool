//
//  ZWRecordTextTool.h
//  Copyright © 2020 ZhangWei. All rights reserved.

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

FOUNDATION_EXPORT double ZWRecordTextToolVersionNumber;
FOUNDATION_EXPORT const unsigned char ZWRecordTextToolVersionString[];

/**
 * 文本文件名命令的时间间隔类型，太大可能会导致单个文件Size很大。
 * 文件名的格式是以时间为间隔的，默认5分钟，如2018-07-19 [19-00~19-05)
 * 最后两段数字表示时间段，比如：[19-00~19-05)表示从19:00到19:05这段时间的日志,包含19:00时刻，不包含19:05时刻
 */
typedef NS_ENUM(NSInteger, SingleRecordFileDurationType)
{
    SingleRecordFileDurationType_5Minutes = 0,//默认值 生成的文件名样式：2018-07-19 [19:00~19:05)
    SingleRecordFileDurationType_1Minutes,    //      生成的文件名样式：2018-07-19 [19:00~19:01)
    SingleRecordFileDurationType_10Minutes,   //      生成的文件名样式：2018-07-19 [19:00~19:10)
    SingleRecordFileDurationType_15Minutes,   //      生成的文件名样式：2018-07-19 [19:00~19:15)
    SingleRecordFileDurationType_20Minutes,   //      生成的文件名样式：2018-07-19 [19:00~19:20)
    SingleRecordFileDurationType_30Minutes,   //      生成的文件名样式：2018-07-19 [19:00~19:30)
    SingleRecordFileDurationType_1Hour,       //      生成的文件名样式：2018-07-19 [19:00~20:00)
    SingleRecordFileDurationType_1Day,        //      生成的文件名样式：2018-07-19
    SingleRecordFileDurationTypeCount,
};

@interface ZWRecordTextTool : NSObject

/**
 * 单个文件持续时长类型，默认SingleRecordFileDurationType_5Minutes。
 * 如需变更请在执行【recordText:/recordData:】前配置。
 */
@property (class, nonatomic, assign) SingleRecordFileDurationType durationType;

/**
 * 文件在沙盒的存储路径。
 * 默认在Library路径，支持变更。
 */
@property (class, nonatomic, copy) NSString *savedDirectory;

/**
 *存储路径下的【类名】文件夹目录，单独生成此类名文件夹，防止对Library路径造成文件污染。
 *即所有的文本文件保存到沙盒Library->ZWRecordTextTool文件夹中并以"年-月-日 [时-分~时-分).txt"命名文件。
 */
@property (class, nonatomic, copy, readonly) NSString *savedFoldPath;

/**
 * 开始记录文字或数据。
 * 数据所被记录的文件与当前设备时间、间隔类型有关
 */
+ (void)recordText:(NSString *)text completedHanle:(void(^)(BOOL result, NSString *filePath))hanle;
+ (void)recordData:(NSData *)data completedHanle:(void(^)(BOOL result, NSString *filePath))hanle;

/**
 * 将文本写入到指定的文件中
 * ⚠️fileName为【nil】、【@""】、或者【非法参数】时，存储文件将使用默认的命名方式，即时间格式。
 */
+ (void)recordText:(NSString *)text fileName:(NSString *)fileName completedHanle:(void(^)(BOOL result, NSString *filePath))hanle;
+ (void)recordData:(NSData *)data fileName:(NSString *)fileName completedHanle:(void(^)(BOOL result, NSString *filePath))hanle;

/**
 * 清除日志文件，包含文件夹。
 */
+ (void)clearFilesWithCompletedHanle:(void(^)(BOOL result))hanle;

/**
 * 根据日期获取文件保存的路径。
 * ⚠️如果获取文件路径前变更了durationType的值，可能会导致获取的路径不准确。
 */
+ (NSString *)filePathWithDate:(NSDate *)date;

/**
 * 根据日期获取文件名。
 * ⚠️如果获取文件名前变更了durationType的值，可能会导致获取的文件名不准确。
 */
+ (NSString *)fileNameWithDate:(NSDate *)date;

@end
