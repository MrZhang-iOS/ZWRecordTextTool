//
//  ZWRecordTextTool.h
//  Copyright © 2020 ZhangWei. All rights reserved.
//

#import "ZWRecordTextTool.h"

static const char *myQueueName = "com.ZWRecordTextTool.Queue";
static SingleRecordFileDurationType g_SingleRecordFileDurationType = SingleRecordFileDurationType_5Minutes;
static NSString *g_SaveDirector = nil;
#define ZWRecordTextToolInstance [ZWRecordTextTool shareInstance]

@interface ZWRecordTextTool()

@property (nonatomic, strong) dispatch_queue_t myQueue;

@end

@implementation ZWRecordTextTool

+ (void)setSavedDirectory:(NSString *)savedDirectory;
{
    if (savedDirectory && [savedDirectory isKindOfClass:[NSString class]] && savedDirectory.length > 0 && [[self class] isFolderPath:savedDirectory])
    {
        g_SaveDirector = [savedDirectory copy];
    }
}

+ (NSString *)savedDirectory;
{
    if (g_SaveDirector == nil)
    {
        g_SaveDirector = [[self class] libraryDirectory];
    }
    return g_SaveDirector;
}

+ (void)setDurationType:(SingleRecordFileDurationType)durationType;
{
    if (g_SingleRecordFileDurationType != durationType)
    {
        g_SingleRecordFileDurationType = durationType;
    }
}

+ (SingleRecordFileDurationType)durationType;
{
    return g_SingleRecordFileDurationType;
}

+ (NSString *)savedFoldPath;
{
    return [[self class] getFolderPathInDirectory:[[self class] savedDirectory]
                                       folderName:NSStringFromClass([self class])];;
}

+ (void)recordText:(NSString *)text completedHanle:(void(^)(BOOL result, NSString *filePath))hanle;
{
    [[self class] recordText:text fileName:nil completedHanle:hanle];
}

+ (void)recordData:(NSData *)data completedHanle:(void(^)(BOOL result, NSString *filePath))hanle;
{
    [[self class] recordData:data fileName:nil completedHanle:hanle];
}

+ (void)clearFilesWithCompletedHanle:(void(^)(BOOL result))hanle;
{
    [ZWRecordTextToolInstance privateToClearFilesWithCompletedHanle:hanle];
}

+ (void)recordText:(NSString *)text fileName:(NSString *)fileName completedHanle:(void(^)(BOOL result, NSString *filePath))hanle;
{
    [[self class] recordData:[text dataUsingEncoding:NSUTF8StringEncoding] fileName:fileName completedHanle:hanle];
}

+ (void)recordData:(NSData *)data fileName:(NSString *)fileName completedHanle:(void(^)(BOOL result, NSString *filePath))hanle;
{
    [ZWRecordTextToolInstance privateToRecordData:data fileName:fileName completedHanle:hanle];
}

+ (NSString *)filePathWithDate:(NSDate *)date;
{
    return [[ZWRecordTextTool savedFoldPath] stringByAppendingPathComponent:[[self class] fileNameWithDate:date]];
}

//⚠️电脑上的文件命令不能包含英文冒号（已在模拟器上亲测，英文冒号会被斜杠代替），iOS设备支持文件命名中包含冒号
+ (NSString *)fileNameWithDate:(NSDate *)date;
{
    if (date == nil || ![date isKindOfClass:[NSDate class]])
    {
        date = [NSDate date];
    }
    
    NSDateComponents *dateComponents = [[self class] dateComponents:date];
    switch ([[self class] durationType])
    {
        case SingleRecordFileDurationType_1Minutes:
        {
            return [NSString stringWithFormat:@"%04ld-%02ld-%02ld [%02ld-%02ld~%02ld-%02ld).txt",
                    (long)dateComponents.year,
                    (long)dateComponents.month,
                    (long)dateComponents.day,
                    (long)dateComponents.hour,
                    (long)dateComponents.minute,
                    (long)dateComponents.hour,
                    (long)(dateComponents.minute + 1)];
        }
            break;
        case SingleRecordFileDurationType_10Minutes:
        {
            return [NSString stringWithFormat:@"%04ld-%02ld-%02ld [%02ld-%02ld~%02ld-%02ld).txt",
                    (long)dateComponents.year,
                    (long)dateComponents.month,
                    (long)dateComponents.day,
                    (long)dateComponents.hour,
                    (long)(dateComponents.minute/10)*10,
                    (long)dateComponents.hour,
                    (long)((dateComponents.minute/10)*10 + 10)];
        }
            break;
        case SingleRecordFileDurationType_15Minutes:
        {
            return [NSString stringWithFormat:@"%04ld-%02ld-%02ld [%02ld-%02ld~%02ld-%02ld).txt",
                    (long)dateComponents.year,
                    (long)dateComponents.month,
                    (long)dateComponents.day,
                    (long)dateComponents.hour,
                    (long)(dateComponents.minute/15)*15,
                    (long)dateComponents.hour,
                    (long)((dateComponents.minute/15)*15 + 15)];
        }
            break;
        case SingleRecordFileDurationType_20Minutes:
        {
            return [NSString stringWithFormat:@"%04ld-%02ld-%02ld [%02ld-%02ld~%02ld-%02ld).txt",
                    (long)dateComponents.year,
                    (long)dateComponents.month,
                    (long)dateComponents.day,
                    (long)dateComponents.hour,
                    (long)(dateComponents.minute/20)*20,
                    (long)dateComponents.hour,
                    (long)((dateComponents.minute/20)*20 + 20)];
        }
            break;
        case SingleRecordFileDurationType_30Minutes:
        {
            return [NSString stringWithFormat:@"%04ld-%02ld-%02ld [%02ld-%02ld~%02ld-%02ld).txt",
                    (long)dateComponents.year,
                    (long)dateComponents.month,
                    (long)dateComponents.day,
                    (long)dateComponents.hour,
                    (long)(dateComponents.minute/30)*30,
                    (long)dateComponents.hour,
                    (long)((dateComponents.minute/30)*30 + 30)];
        }
            break;
        case SingleRecordFileDurationType_1Hour:
        {
            return [NSString stringWithFormat:@"%04ld-%02ld-%02ld [%02ld-00~%02ld-00).txt",
                    (long)dateComponents.year,
                    (long)dateComponents.month,
                    (long)dateComponents.day,
                    (long)dateComponents.hour,
                    (long)dateComponents.hour+1];
        }
            break;
        case SingleRecordFileDurationType_1Day:
        {
            return [NSString stringWithFormat:@"%04ld-%02ld-%02ld.txt",
                    (long)dateComponents.year,
                    (long)dateComponents.month,
                    (long)dateComponents.day];
        }
            break;
        case SingleRecordFileDurationType_5Minutes:
        default:
        {
            return [NSString stringWithFormat:@"%04ld-%02ld-%02ld [%02ld-%02ld~%02ld-%02ld).txt",
                    (long)dateComponents.year,
                    (long)dateComponents.month,
                    (long)dateComponents.day,
                    (long)dateComponents.hour,
                    (long)(dateComponents.minute/5)*5,
                    (long)dateComponents.hour,
                    (long)((dateComponents.minute/5)*5 + 5)];
        }
            break;
    }
}

#pragma mark - Private Function
+ (NSString *)fileName
{
    return [[self class] fileNameWithDate:nil];
}

+ (NSString *)libraryDirectory;
{
    return [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) lastObject];
}

+ (BOOL)isFileExistsAtPath:(NSString *)filePath;
{
    if (filePath && [filePath isKindOfClass:[NSString class]] && filePath.length > 0)
    {
        return [[NSFileManager defaultManager] fileExistsAtPath:filePath];
    }
    return NO;
}

+ (BOOL)isFolderPath:(NSString *) folderPath;
{
    BOOL isFolder = NO;
    if (folderPath && [folderPath isKindOfClass:[NSString class]] && folderPath.length > 0)
    {
        [[NSFileManager defaultManager] fileExistsAtPath:folderPath isDirectory:&isFolder];
        return isFolder;
    }
    return isFolder;
}

+ (BOOL)createFolderInDirectory:(NSString *)directoryPath folderName:(NSString *)folderName;
{
    if ([[self class] isFileExistsAtPath:directoryPath] == NO)
    {
        NSLog(@"路径不存在【%@】", directoryPath);
        return NO;
    }
    if (folderName.length <= 0)
    {
        return YES;
    }
    NSArray *pSubFolders = [folderName componentsSeparatedByString:@"/"];
    BOOL bPathValid = YES;
    if (pSubFolders && [pSubFolders count])
    {
        for (NSString* pTempPath in pSubFolders)
        {
            if (pTempPath.length <= 0)
            {
                bPathValid = NO;
                break;
            }
        }
    }
    else
    {
        bPathValid = NO;
    }
    if (!bPathValid)
    {
        NSLog(@"create folder failed, folderName is invalid, 部分子文件夹名字的长度小于或等于0,【%@】", folderName);
        return NO;
    }
    
    NSString *pFolderPath = [NSString stringWithString:directoryPath];
    NSError *pError = nil;
    BOOL bCreateResult = YES;
    for (NSString *pSubFolderName in pSubFolders)
    {
        pFolderPath = [pFolderPath stringByAppendingPathComponent:pSubFolderName];
        if ([[self class] isFileExistsAtPath:pFolderPath] == NO || [[self class] isFolderPath:pFolderPath] == NO)
        {
            //createIntermediates是一个BOOL值，表示是否创建中间路径。YES为创建(无论是否已经存在)，NO为不创建。
            //例如：参数createIntermediates为【my_work/important_work】，即需要在【my_work】文件夹下创建一个【important_work】文件夹。多级目录也是如此。
            //当【my_work】文件夹不存在，
            //①设置createIntermediates=YES时，则【my_work】和【important_work】文件夹都会被创建，
            //②若设置createIntermediates=NO时，则【my_work】和【important_work】文件夹都不会被创建。
            //当【my_work】文件夹已经存在，createIntermediates的值为YES或者NO，都不影响【important_work】文件夹的创建。
            bCreateResult = [[NSFileManager defaultManager] createDirectoryAtPath:pFolderPath withIntermediateDirectories:YES attributes:nil error:&pError];
            if (!bCreateResult)
            {
                NSLog(@"create folder failed, Error message【%@】", [pError localizedDescription]);
                break;
            }
        }
    }
    return bCreateResult;
}

+ (NSString *)getFolderPathInDirectory:(NSString *)directoryPath folderName:(NSString *)folderName;
{
    if ([[self class] isFileExistsAtPath:directoryPath] == NO)
    {
        NSLog(@"路径不存在【%@】", directoryPath);
        return nil;
    }
    //遇到了崩溃，日志如下 -[__NSMallocBlock__ stringByAppendingPathComponent:]: unrecognized selector sent to
    if ([directoryPath respondsToSelector:@selector(stringByAppendingPathComponent:)])
    {
        NSString *pFolderPath = [directoryPath stringByAppendingPathComponent:folderName];
        if ([[self class] isFileExistsAtPath:pFolderPath] && [[self class] isFolderPath:pFolderPath])
        {
            return pFolderPath;
        }
        else
        {
            if ([[self class] createFolderInDirectory:directoryPath folderName:folderName])
            {
                return pFolderPath;
            }
            else
            {
                return nil;
            }
        }
    }
    return nil;
}

+ (instancetype)shareInstance
{
    static ZWRecordTextTool *g_Instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        g_Instance = [[ZWRecordTextTool alloc] init];
    });
    
    return g_Instance;
}

- (instancetype)init
{
    if (self = [super init])
    {
        /// 创建自定义的并发队列，否则栅栏无效。
        /// iOS6.0及以后，GCD对象已经纳入了ARC的管理范围，不需要再手工调用 dispatch_release了。
        _myQueue = dispatch_queue_create(myQueueName, DISPATCH_QUEUE_CONCURRENT);
    }
    return self;
}

- (void)privateToRecordData:(NSData *)data fileName:(NSString *)fileName completedHanle:(void(^)(BOOL result, NSString *filePath))hanle;
{
    dispatch_barrier_async(_myQueue, ^{
        BOOL result = NO;
        
        NSString *filePath = [ZWRecordTextTool filePathWithDate:nil];
        if (fileName && [fileName isKindOfClass:[NSString class]] && fileName.length > 0)
        {
            filePath = [[ZWRecordTextTool savedFoldPath] stringByAppendingPathComponent:fileName];
        }
         
        if (data && [data isKindOfClass:[NSData class]])
        {
            NSFileManager *fileManager = [NSFileManager defaultManager];

            if(![fileManager fileExistsAtPath:filePath])
            {
                //文件不存在
                @try
                {
                    result = [data writeToFile:filePath atomically:YES];
                }
                @catch(NSException *exception)
                {
                    result = NO;
                }
            }
            else
            {
                //文件已存在
                result = YES;
                NSFileHandle *fileHandle = [NSFileHandle fileHandleForWritingAtPath:filePath];
                [fileHandle seekToEndOfFile];
                
                @try
                {
                    [fileHandle writeData:data];
                }
                @catch(NSException *exception)
                {
                    result = NO;
                }
                
                [fileHandle closeFile];
            }
        }
        
        if (hanle)
        {
            hanle(result, filePath);
        }
    });
}

- (void)privateToClearFilesWithCompletedHanle:(void(^)(BOOL result))hanle;
{
    dispatch_barrier_async(_myQueue, ^{
        BOOL result = NO;
        
        NSString *savedFoldPath = [ZWRecordTextTool savedFoldPath];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        if(![fileManager fileExistsAtPath:savedFoldPath])
        {
            //文件不存在
            result = YES;
        }
        else
        {
            //文件已存在
            result = YES;
            @try
            {
                [fileManager removeItemAtPath:savedFoldPath error:nil];
            }
            @catch(NSException *exception)
            {
                result = NO;
            }
        }
        
        if (hanle)
        {
            hanle(result);
        }
    });
}

+ (NSDateComponents *)dateComponents:(NSDate *)date;
{
    static NSCalendar *g_CalendarInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        g_CalendarInstance = [NSCalendar autoupdatingCurrentCalendar];
    });
    NSDateComponents *components = [g_CalendarInstance components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay |  NSCalendarUnitHour | NSCalendarUnitMinute ) fromDate:date];
    return components;
}

@end
