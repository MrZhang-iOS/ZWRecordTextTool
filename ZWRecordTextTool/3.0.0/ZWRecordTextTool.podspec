Pod::Spec.new do |spec|
  spec.name                  = "ZWRecordTextTool"
  spec.version               = "3.0.0"
  spec.summary               = "用来在App沙盒Library路径下记录文字，会生成根据时间命名的文本文件"
  spec.description           = "可用来记录一些重要日志等，支持存储路径变更、修改时间间隔的方式保存文件"
  spec.homepage              = "https://gitee.com/MrZhang-iOS/ZWRecordTextTool"
  spec.license               = { :type => "MIT", :file => "FILE_LICENSE" }
  spec.author                = { "MrZhang-iOS" => "why.20130509@gmail.com" }
  spec.source                = { :git => "https://gitee.com/MrZhang-iOS/ZWRecordTextTool.git", :tag => spec.version.to_s }
  spec.source_files          = "trunk/ZWRecordTextTool/*.{h,m}"
  spec.public_header_files   = "trunk/ZWRecordTextTool/*.h"
  spec.ios.deployment_target = "12.0"
  spec.frameworks            = "UIKit", "Foundation"
end
